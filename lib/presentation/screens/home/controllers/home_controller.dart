import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  final inputController = TextEditingController();
  final digitAndPosition = [].obs;

  findTheDigit(var digit) {
    if (digit.isNotEmpty) {
      digit = int.parse(digit);

      var sequence = [];
      for (var i = 1; i <= digit; i++) {
        var digitNumber = i.toString().split('').toList();
        for (var d in digitNumber) {
          sequence.add([int.parse(d), i]);
        }
      }
      digitAndPosition.value = sequence[digit - 1];
    } else {
      digitAndPosition.clear();
    }
  }
}
