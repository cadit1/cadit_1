import 'package:cadit_1/_core/constants/app_constant.dart';
import 'package:cadit_1/_core/extensions.dart';
import 'package:cadit_1/presentation/_core/app_color.dart';
import 'package:cadit_1/presentation/_core/app_text_style.dart';
import 'package:cadit_1/presentation/screens/home/controllers/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({super.key});
  final _controller = Get.find<HomeController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColor.primary500,
        title: Text(
          'CAD IT Number 1 Test',
          style: AppTextStyle.titleLarge.copyWith(color: AppColor.neutral1),
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(AppConstant.kDefaultPadding),
        child: Column(
          children: [
            TextField(
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                label: const Text('Digit'),
                labelStyle:
                    AppTextStyle.bodyLarge.copyWith(color: AppColor.primary500),
                focusedBorder: const UnderlineInputBorder(
                  borderSide: BorderSide(color: AppColor.primary500),
                ),
              ),
              cursorColor: AppColor.primary500,
              onChanged: (value) {
                _controller.findTheDigit(value);
              },
              maxLength: 6,
              controller: _controller.inputController,
            ),
            const SizedBox(
              height: AppConstant.kDefaultPadding * 2,
            ),
            Obx(() {
              return _controller.digitAndPosition.isNotEmpty
                  ? RichText(
                      text: TextSpan(
                        text: _controller.inputController.text.toNumberRank,
                        style: AppTextStyle.bodyLarge
                            .copyWith(color: AppColor.neutral13),
                        children: [
                          const TextSpan(text: ' digit number is '),
                          TextSpan(
                            text: '${_controller.digitAndPosition[0]} ',
                            style: AppTextStyle.bodyLarge.copyWith(
                              color: AppColor.neutral13,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const TextSpan(text: 'lies on number '),
                          TextSpan(
                            text: '${_controller.digitAndPosition[1]}',
                            style: AppTextStyle.bodyLarge.copyWith(
                              color: AppColor.neutral13,
                              fontWeight: FontWeight.bold,
                            ),
                          )
                        ],
                      ),
                    )
                  : Container();
            })
          ],
        ),
      ),
    );
  }
}
