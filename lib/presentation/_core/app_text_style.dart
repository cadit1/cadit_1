import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppTextStyle {
  //DISPLAY
  static final displayLarge = GoogleFonts.nunitoSans(
    textStyle: const TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 57.0,
      height: 64 / 57,
    ),
  );
  static final displayMedium = GoogleFonts.nunitoSans(
    textStyle: const TextStyle(
      fontWeight: FontWeight.w400,
      fontSize: 45.0,
      height: 52 / 45,
    ),
  );
  static final displaySmall = GoogleFonts.nunitoSans(
    textStyle: const TextStyle(
      fontWeight: FontWeight.w400,
      fontSize: 36.0,
      height: 44 / 36,
    ),
  );

  //HEADLINE
  static final headlineLarge = GoogleFonts.nunitoSans(
    textStyle: const TextStyle(
      fontWeight: FontWeight.w700,
      fontSize: 32.0,
      height: 40 / 32,
    ),
  );
  static final headlineMedium = GoogleFonts.nunitoSans(
    textStyle: const TextStyle(
      fontWeight: FontWeight.w400,
      fontSize: 28.0,
      height: 36 / 28,
    ),
  );
  static final headlineSmall = GoogleFonts.nunitoSans(
    textStyle: const TextStyle(
      fontWeight: FontWeight.w400,
      fontSize: 24.0,
      height: 32 / 24,
    ),
  );

  //TITLE
  static final titleLarge = GoogleFonts.nunitoSans(
    textStyle: const TextStyle(
      fontWeight: FontWeight.w700,
      fontSize: 20.0,
      height: 28 / 20,
    ),
  );
  static final titleMedium = GoogleFonts.nunitoSans(
    textStyle: const TextStyle(
      fontWeight: FontWeight.w700,
      fontSize: 16.0,
      height: 24 / 16,
    ),
  );
  static final titleSmall = GoogleFonts.nunitoSans(
    textStyle: const TextStyle(
      fontWeight: FontWeight.w700,
      fontSize: 14.0,
      height: 20 / 14,
    ),
  );

  //BODY
  static final bodyLarge = GoogleFonts.nunitoSans(
    textStyle: const TextStyle(
      fontSize: 16.0,
      height: 24 / 16,
    ),
  );
  static final bodyMedium = GoogleFonts.nunitoSans(
    textStyle: const TextStyle(
      fontWeight: FontWeight.w600,
      fontSize: 14.0,
      height: 20 / 14,
    ),
  );
  static final bodySmall = GoogleFonts.nunitoSans(
    textStyle: const TextStyle(
      fontWeight: FontWeight.w400,
      fontSize: 12.0,
      height: 16 / 12,
    ),
  );

  //LABEL
  static final labelLarge = GoogleFonts.nunitoSans(
    textStyle: const TextStyle(
      fontWeight: FontWeight.w600,
      fontSize: 14.0,
      height: 20 / 14,
    ),
  );
  static final labelMedium = GoogleFonts.nunitoSans(
    textStyle: const TextStyle(
      fontWeight: FontWeight.w600,
      fontSize: 12.0,
      height: 16 / 12,
    ),
  );
  static final labelSmall = GoogleFonts.nunitoSans(
    textStyle: const TextStyle(
      fontWeight: FontWeight.w400,
      fontSize: 11.0,
      height: 16 / 11,
    ),
  );

  //BUTTON
  static final buttonLarge = GoogleFonts.nunitoSans(
    textStyle: const TextStyle(
      fontWeight: FontWeight.w700,
      fontSize: 16.0,
      height: 21.82 / 16,
    ),
  );

  static final buttonMedium = GoogleFonts.nunitoSans(
    textStyle: const TextStyle(
      fontWeight: FontWeight.w600,
      fontSize: 14.0,
      height: 19.1 / 16,
    ),
  );

  static final buttonSmall = GoogleFonts.nunitoSans(
    textStyle: const TextStyle(
      fontWeight: FontWeight.w600,
      fontSize: 12.0,
      height: 16.37 / 12,
    ),
  );

  //HEADING
  static final subHeading = GoogleFonts.montserrat(
    textStyle: const TextStyle(
      fontWeight: FontWeight.w500,
      fontSize: 12.0,
      height: 14.63 / 12,
    ),
  );

  //QUOTE
  static final titleQuotes = GoogleFonts.ibmPlexMono(
    textStyle: const TextStyle(
      fontWeight: FontWeight.w700,
      fontSize: 20.0,
      height: 26 / 20,
    ),
  );
}
