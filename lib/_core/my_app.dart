import 'package:cadit_1/_core/constants/app_page.dart';
import 'package:cadit_1/_core/initial_binding.dart';
import 'package:cadit_1/presentation/_core/app_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'CADIT Number 1 Test',
      theme: ThemeData(
        primarySwatch: AppColor.primary,
        scaffoldBackgroundColor: Colors.white,
        useMaterial3: false,
      ),
      initialBinding: InitialBinding(),
      getPages: AppPage.pages,
      locale: const Locale('id', 'ID'),
    );
  }
}
