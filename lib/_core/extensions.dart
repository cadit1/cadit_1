extension NumberX on String {
  String get toNumberRank {
    if (isEmpty) {
      return '';
    }
    switch (int.parse(this) % 10) {
      case 1:
        return '$this' 'st';
      case 2:
        return '$this' 'nd';
      case 3:
        return '$this' 'rd';
      default:
        return '$this' 'th';
    }
  }
}
