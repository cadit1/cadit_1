import 'package:cadit_1/presentation/presentation_binding.dart';
import 'package:get/get.dart';

class InitialBinding extends Bindings {
  @override
  void dependencies() {
    // DataBinding().dependencies();
    // DomainBinding().dependencies();
    PresentationBinding().dependencies();
  }
}
