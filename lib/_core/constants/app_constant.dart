class AppConstant {
  static const kConnectionTimeout = 30000;
  static const kDefaultPadding = 16.0;
}
