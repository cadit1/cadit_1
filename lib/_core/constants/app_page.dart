import 'package:cadit_1/presentation/screens/home/home_binding.dart';
import 'package:cadit_1/presentation/screens/home/screens/home_screen.dart';
import 'package:get/get.dart';

class AppPage {
  static const homeScreen = '/';

  static var pages = <GetPage>[
    //HOME
    GetPage(
      name: homeScreen,
      page: () => HomeScreen(),
      binding: HomeBinding(),
    ),
  ];
}
